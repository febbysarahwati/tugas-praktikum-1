package com.febbysarahwati_10191029.praktikum1.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.febbysarahwati_10191029.praktikum1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}